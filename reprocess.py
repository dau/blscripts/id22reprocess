# Usage:
#
# On diffract22new:
#
#  conda activate ewoksworker
#  python reprocess.py
#

from ewoks import execute_graph, convert_graph
from glob import glob
import os
import logging


logging.basicConfig(level=logging.INFO)

if __name__ == "__main__":
    run_name = "reprocess1"
    poni_filename = "/data/visitor/ch6690/id22/20240220/PROCESSED_DATA/processing_2D/LaB6_60keV_tfin/.azimint.json"

    for filename in glob(
        "/data/visitor/ch6690/id22/20240220/PROCESSED_DATA/*/*/*.json"
    ):
        parts = filename.split(os.sep)
        sample = parts[-3]
        sample_dataset = parts[-2]
        if sample == "workflows":
            continue
        if "cold" not in sample.lower():
            continue

        wf = dict()
        wf = convert_graph(filename, wf)
        for node in wf["nodes"]:
            default_inputs = node.get("default_inputs", list())
            for param in default_inputs:
                if "PROCESSED_DATA" in str(param["value"]):
                    if param["value"].endswith(".json"):
                        param["value"] = poni_filename
                    else:
                        param["value"] = param["value"].replace(
                            "PROCESSED_DATA", f"PROCESSED_DATA/{run_name}"
                        )

        convert_destination = filename.replace(
            "PROCESSED_DATA", f"PROCESSED_DATA/{run_name}"
        )

        execute_graph(wf, convert_destination=convert_destination)
